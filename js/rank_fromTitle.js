/*
 * global settings
 */
var lambdaFunctionUrl = "https://qg3weuapwd.execute-api.us-west-2.amazonaws.com/UrlListRankerStage/urllistrankerresource";
var rankResultsDivId = "rank_result";
var redditDivId = "stage";
var redditId = "reddit";
var customId = "custom";
var defaultWidgetWidth = 250;
var defaultNumColumns = 4;

/*
 * global widget arrays
 */
var responseWidgetArray = [];
var redditWidgetArray = [];

/*
 * Api call from text input field
 */
function sendUrltoLambda() {
    getResponseFromLambda();
}

/*
 * Api call for reddit feed
 */
function showRedditArticles() {
    getRedditWidgets();
}

/*
 * runs reddit articles through the url validator lambda
 */
function getRedditWidgets() {
    $.getJSON('https://www.reddit.com/r/news/.json', function (data) {
        $('#stage').html('<h1>Loading...</h1>');

        var getParams = '?requestUrls=[';
        var articles = data.data.children;
        for(var i = 0; i < articles.length; i++) {
            getParams += '"' + articles[i].data.domain + '"';
            if(i != articles.length-1) {
                getParams += ',';
            }
        }
        getParams += ']';
        var completeLambdaUrl = lambdaFunctionUrl + getParams;
        var xhttp = new XMLHttpRequest();
        xhttp.timeout = 20000; 
        xhttp.open("GET", completeLambdaUrl, true);
        xhttp.onreadystatechange = function() {
            if (xhttp.readyState == 4 && xhttp.status == 200) {
                var obj = JSON.parse(xhttp.responseText);
                rank = obj;
                for(var i = 0; i < articles.length; i++) {
                    redditWidgetArray.push(new NewsArticle(redditId, articles[i].data.title, articles[i].data.url, articles[i].data.domain, rank[i], i));
                }
                writeWidgets('<h1>Trending Reddit articles</h1>',redditWidgetArray, redditDivId);
            } 
        };
        xhttp.send();
    });
}

/*
 * Calls UrlListRank lambda with a list of urls.  The lambda responds with a list of ranks in the same order as the URLs.
 */
function getResponseFromLambda() {
    var requestedUrl = document.getElementById("urlname").value;
    var completeLambdaUrl = lambdaFunctionUrl + '?requestUrls=["' + requestedUrl + '"]';
    var xhttp = new XMLHttpRequest();
    xhttp.timeout = 20000; 
    xhttp.open("GET", completeLambdaUrl, true);
    xhttp.onreadystatechange = function() {
        if (xhttp.readyState == 4 && xhttp.status == 200) {
            var obj = JSON.parse(xhttp.responseText);
            var rank = obj[0];
            responseWidgetArray.push(new NewsArticle(customId, null, requestedUrl, requestedUrl, rank, responseWidgetArray.length));
            writeWidgets('', responseWidgetArray, rankResultsDivId);
        } 
    };
    xhttp.send();
}


/**
 * writes widgets from widgetArray to the html div element with divId
 */
function writeWidgets(title, widgetArray, divId) {
    var htmlOutput = title + '<table cellspacing=20><tr>';
    for(var i = 0; i < widgetArray.length; i++) {
        if(i % defaultNumColumns == 0) {
            htmlOutput += '</tr><tr>';
        }
        htmlOutput += '<td>' + widgetArray[i].widgetHtml + '</td>';
    }
    htmlOutput += '</tr></table><hr>';
    document.getElementById(divId).innerHTML = htmlOutput;
}

/**
 * News Article object conatins info about a news article and it's html representation
 */
function NewsArticle(in_widgetCollection, in_title, in_url, in_source, in_rank, in_index) {
	this.collection = in_widgetCollection;
    this.title = in_title;
    this.url = in_url;
    this.source = in_source;
    this.rank = in_rank;
    this.index = in_index;
    var colorString = getColor(this.rank);
    var colorStyle = colorString + 'widget';
    var titleText;
    if(this.title) {
        titleText = this.title;
    } else {
        titleText = this.url;
    }
    this.widgetHtml = '<table class="widget ' + colorStyle + '" border=0 width=' + defaultWidgetWidth + '>' +
            '<tr><td onclick="open_sidebar(\''+ this.collection +'\', ' + this.index + ')">' +
            titleText + '</a><br><br>' +
            'Source: ' + this.source + '<br>' +
            'Reliability index: ' + this.rank + '%' +
            '</td><td><button id="upvote">Up</button><button id="downvote">Down</button></td></tr></table>'; 
}

function getColor(rank) {
    var colorString = "green";
    if(rank < 70) {
        colorString = "yellow";
    }
    if(rank < 30) {
        colorString = "red";
    }
    return colorString;
}

/*
 * deploys sidebar and displays the url contained in widgetCollection at the specified index.
 * e.g. if widgetCollection == reddit and index == 2, it will bring up the url of the third 
 * reddit widget.
 */
function open_sidebar(widgetCollection, widgetIndex) {
    document.getElementById("target-sidebar").style.width = "90%";
    document.getElementById("target-sidebar").style.display = "block";
    var article = getNewsArticle(widgetCollection, widgetIndex);
    document.getElementById("content-frame").src = article.url;
    document.getElementById("sidebar-info").innerHTML = "Reliability index: " + article.rank + "%";  
    document.getElementById("sidebar-header").style.backgroundColor = getColor(article.rank);
}
/*
 * Collapses the sidebar
 */
function close_sidebar() {
    document.getElementById("target-sidebar").style.display = "none";
}
/*
 * retrieves the NewsArticle object from the spcified collection at the specified index
 */
function getNewsArticle(widgetCollection, index) {
	if(widgetCollection == "reddit") {
		return redditWidgetArray[index];
	} 
	return responseWidgetArray[index];
}

//listener for the upvote button
$(document).on("click","#upvote",function(){
    var url = $(this).closest('tr').children('td:first').children('a').attr('href');
    alert("You are going to upvote for " + url);
    updateRank(url, true);
});

//listener for the downvote button
$(document).on("click","#downvote",function(){
    var url = $(this).closest('tr').children('td:first').children('a').attr('href');
    alert("You are going to downvote for " + url);
    updateRank(url, false);
});

//method to call the Gateway Api
var rankGatewayUrl = "";
function updateRank(url, vote){
    // do later
}
